import React, { createRef } from "react";
import { Map, Popup, WMSTileLayer, AttributionControl } from "react-leaflet";
// eslint-disable-next-line
import S from "leaflet-css";

// Used for sett key= on popups
let numMapClicks = 0;

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 60.39249,
      lng: 5.31786,
      zoom: 17,
      scrollWheelZoom: true,
      doubleClickZoom: true,
      attributionControl: false,
      maxZoom: 19,
      matrikkel: "",
      addressString: "",
      postalCode: "",
      formattedAddressString: ""
    };
    this.handleResponse = this.handleResponse.bind(this);
  }

  refmarker = createRef();

  // Get the first property in the list of properties returned from Kartverket REST api
  getProperty = (latitude, longtitude, searchRadius = 20) => {
    let that = this;
    console.log(
      `GET matrikkel for ${latitude.toFixed(5)} / ${longtitude.toFixed(5)}`
    );

    let url = `https://ws.geonorge.no/adresser/v1/punktsok?radius=${searchRadius}&lat=${latitude}&lon=${longtitude}&treffPerSide=10&side=0`;
    const request = new XMLHttpRequest();
    request.responseType = "json";
    request.open("GET", url, true);
    request.onload = function() {
      if (this.status === 200) {
        that.handleResponse(request.response);
      }
    };
    request.send();
  };

  // Handle response from matrikkelen
  handleResponse = response => {
    if (response.metadata.totaltAntallTreff !== 0) {
      this.setState({
        addressString: `${response.adresser["0"].adressenavn} ${
          response.adresser["0"].nummer
        }, ${response.adresser["0"].postnummer} ${
          response.adresser["0"].poststed
        }`,
        postalCode: response.adresser["0"].postnummer,
        formattedAddressString: `${response.adresser["0"].adressenavn} ${
          response.adresser["0"].nummer
        }, ${response.adresser["0"].poststed}`
      });

      let mnr = response.adresser["0"].kommunenummer;
      let gnr = response.adresser["0"].gardsnummer;
      let bnr = response.adresser["0"].bruksnummer;
      let fnr = response.adresser["0"].festenummer;
      this.setState({
        matrikkel: `${mnr}-${gnr}-${bnr}`,
        gnr: gnr,
        bnr: bnr,
        fnr: fnr
      });
    } else {
      console.log("No address returned or invalid format.");
    }
  };

  // Add Popup on mouse click
  addPopup = event => {
    this.setState({
      popup: {
        key: numMapClicks++,
        position: event.latlng
      }
    });
    this.getProperty(event.latlng.lat, event.latlng.lng);
  };

  // Handle button click inside Popup
  handleClick = event => {
    let data = {
      address: this.state.formattedAddressString,
      postalCode: this.state.postalCode,
      matrikkel: this.state.matrikkel,
      gnr: this.state.gnr,
      bnr: this.state.bnr,
      fnr: this.state.fnr
    };
    // console.log(this);
    // console.log(this.props);
    this.props.callBackFromParent(data);
  };

  render() {
    const mapCenter = [this.state.lat, this.state.lng];
    const { popup } = this.state;

    return (
      <Map
        center={mapCenter}
        zoom={this.state.zoom}
        maxZoom={this.state.maxZoom}
        scrollWheelZoom={this.state.scrollWheelZoom}
        doubleClickZoom={this.state.doubleClickZoom}
        attributionControl={this.state.attributionControl}
        onClick={this.addPopup}
      >
        <AttributionControl position="bottomright" prefix="" />
        <WMSTileLayer
          attribution='<a href="https://kartkatalog.geonorge.no/metadata/kartverket/norges-grunnkart-gratone-cache/dd4a230e-0d02-4514-bab4-c7c279040c8c">Kartverket Gråtoner</a>'
          url="http://opencache.statkart.no/gatekeeper/gk/gk.open_gmaps?layers=norges_grunnkart_graatone&zoom={z}&x={x}&y={y}"
        />
        <WMSTileLayer
          format="image/png"
          maxZoom="20"
          transparent="true"
          opacity="0.7"
          attribution='<a href="https://kartkatalog.geonorge.no/metadata/kartverket/matrikkelen-wms/63c672fa-e180-4601-a176-6bf163e0929d">Matrikkelen</a>'
          url="https://openwms.statkart.no/skwms1/wms.matrikkel?"
          layers="eiendomsgrense,eiendoms_id"
        />

        {popup && (
          <Popup key={`popup-${popup.key}`} position={popup.position}>
            <div>
              <p>
                {/* If popup has coordiantes and something was found */}
                {popup.position && this.state.matrikkel
                  ? `Dette er 
                  ${this.state.addressString} med matrikkelnummer 
                  ${this.state.matrikkel}`
                  : "Ingen adresse funnet"}
              </p>
              <button onClick={this.handleClick}>
                {" "}
                Dette er min adresse. Bruk denne{" "}
              </button>
            </div>
          </Popup>
        )}
      </Map>
    );
  }
}
