import React from "react";
import "./App.css";
import Home from "./Home";

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMap: false,
      showMatrikkelInput: false
    };
    this.toggleMapView = this.toggleMapView.bind(this);
    this.toggleMatrikkelView = this.toggleMatrikkelView.bind(this);
  }

  toggleMapView() {
    this.setState(state => ({
      showMap: !state.showMap
    }));
  }

  toggleMatrikkelView() {
    this.setState(state => ({
      showMatrikkelInput: !state.showMatrikkelInput
    }));
  }

  callback = dataFromChild => {
    this.setState({
      address: dataFromChild.address,
      postalCode: dataFromChild.postalCode,
      matrikkel: dataFromChild.matrikkel,
      gnr: dataFromChild.gnr,
      bnr: dataFromChild.bnr,
      fnr: dataFromChild.fnr
    });
  };

  render() {
    return (
      <div className="App">
        <div className="ff-panel-heading">
          <h2>
            <label>Søk opp huset ditt</label>
          </h2>
        </div>
        <div className="ff-panel-body">
          <div className="ff-input ff-form-group ff-has-feedback ">
            <label id="ff-input-postalCode-label" className="ff-displayname">
              {this.state.showMatrikkelInput
                ? "Søk med matrikkelnummer"
                : "Søk på adresse"}
            </label>
            <div className="ff-form-wrapper">
              {this.state.showMatrikkelInput ? (
                <div id="matrikkel-container">
                  <div className="ff-serial-input">
                    <div className="ff-input ff-form-group ff-has-feedback ">
                      <input
                        type="number"
                        className="ff-form-control"
                        id="ff-input-gnr"
                        placeholder="Gnr"
                        maxLength="4"
                        min="0"
                        max="9999"
                        value={this.state.gnr}
                        spellCheck="false"
                        aria-labelledby="ff-input-gnr-label"
                        pattern="[0-9]*"
                        autoComplete="on"
                        noValidate=""
                      />

                      <input
                        type="number"
                        className="ff-form-control"
                        id="ff-input-bnr"
                        placeholder="Bnr"
                        min="0"
                        max="9999"
                        value={this.state.bnr}
                        maxLength="4"
                        spellCheck="false"
                        aria-labelledby="ff-input-bnr-label"
                        pattern="[0-9]*"
                        autoComplete="on"
                        noValidate=""
                      />

                      <input
                        type="number"
                        className="ff-form-control"
                        id="ff-input-fnr"
                        placeholder="Fnr"
                        value={this.state.fnr}
                        maxLength="4"
                        min="0"
                        max="9999"
                        spellCheck="false"
                        aria-labelledby="ff-input-fnr-label"
                        pattern="[0-9]*"
                        autoComplete="on"
                        noValidate=""
                      />
                    </div>
                  </div>
                </div>
              ) : (
                <div>
                  <input
                    type="number"
                    className="ff-form-control"
                    id="ff-input-postalCode"
                    placeholder="Postnummer"
                    maxLength="4"
                    value={this.state.postalCode}
                    min="0"
                    max="9999"
                    spellCheck="false"
                    aria-labelledby="ff-input-postalCode-label"
                    pattern="[0-9]*"
                    autoComplete="on"
                  />
                  <input
                    type="text"
                    className="address"
                    id="ff-input-searchInput"
                    placeholder="Adresse"
                    spellCheck="false"
                    aria-labelledby="ff-input-searchInput-label"
                    disabled=""
                    value={this.state.address}
                    autoComplete="off"
                  />
                </div>
              )}

              <div>
                <a
                  href="./#"
                  onClick={this.toggleMatrikkelView}
                  class="ff-link ff-toggle-link"
                >
                  Jeg har bare gårds- og bruksnummer
                </a>
              </div>
              <div>
                <a
                  href="./#"
                  onClick={this.toggleMapView}
                  class="ff-link ff-toggle-link"
                >
                  Jeg kan ingenting. Velg fra kart
                </a>
              </div>
            </div>
          </div>

          {this.state.showMap ? (
            <div id="leaflet-container">
              <Home callBackFromParent={this.callback} />
            </div>
          ) : (
            "" //nothing
          )}
          <div className="ff-nav-buttons ff-clearfix">
            <a href="./#" class="ff-btn ff-btn-primary">
              Neste
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default Main;
