# Matrikkelkart

Demonstrasjon av komponent hvor man kan klikke i et kart og få ut matrikkelnummer og/eller adresse, fylt tilbake i søkefeltet. Laget med [react-leaflet](https://github.com/PaulLeCam/react-leaflet) og `create-react-app` og kartdata fra Kartverket. Som standard er kartlagene `Norgeskart gråtoner`, som bakgrunnskart og `Matrikkelen`, for grenser, aktivert.

![skjermbilde](./skjermbilde.gif)

## Oppsett

1. `git clone git@gitlab.com:FredrikLindseth/matrikkelkart.git`
2. Bytt til mappen og `npm install` for å installere
3. `npm start` for å starte nettjeneren
4. Gå til [localhost:3000](http://localhost:3000)
5. Klikk "Jeg kan ingenting. Velg fra kart" og klikk i kartet
6. Få ut adresse og matrikkelnummer (kommunenummer, gårdsnummer, bruksnummer) fylt inn i skjema

Laget til Frende Hackathon 2019

## Bruke

Hvis man skal bruke kartet som komponent i noe annet trenger man bare komponenten `./src/Home.js`. Den forventer å bli kalt som `<Home callBackFromParent={this.callback} />` og få en callback i props ala dette:

```javascript
callback = dataFromChild => {
  this.setState({
    address: dataFromChild.address,
    postalCode: dataFromChild.postalCode,
    matrikkel: dataFromChild.matrikkel,
    gnr: dataFromChild.gnr,
    bnr: dataFromChild.bnr,
    fnr: dataFromChild.fnr
  });
};
```

Komponenten sender ut

- address som er "Gatenavn Husnummer, Poststed"
- postalCode som er "Postnummer"
- gnr som er gårdsnummer
- bnr som er bruksnummer
- fnr som er festenummer
- matrikkel som er "kommunenummer - gårdsnummer - bruksnummer"

## Enkel versjon

I mappen `simple/` ligger en versjon uten react med standard leaflet
