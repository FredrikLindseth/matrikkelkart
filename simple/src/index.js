var map = L.map("map-container", {
  scrollWheelZoom: true,
  doubleClickZoom: true,
  attributionControl: false, //hide "leaflet" in the attribution
  maxZoom: 20,
  zoom: 16,
  center: [60.3923, 5.31732] // The coords of Jonsvoll
});

Norgeskart: L.tileLayer(
  "https://opencache.statkart.no/gatekeeper/gk/gk.open_gmaps?layers=topo4&zoom={z}&x={x}&y={y}",
  {
    id: "topo4",
    maxZoom: 20,
    attribution:
      '<a href="https://kartkatalog.geonorge.no/metadata/kartverket/norgeskart-bakgrunn-cache/c0d063aa-59fc-42db-bc5d-a1c88f2bf256">Kartverket Norgeskart</a>'
  }
);

Gråtone: L.tileLayer(
  "http://opencache.statkart.no/gatekeeper/gk/gk.open_gmaps?layers=norges_grunnkart_graatone&zoom={z}&x={x}&y={y}",
  {
    maxZoom: 20,
    attribution:
      '<a href="https://kartkatalog.geonorge.no/metadata/kartverket/norges-grunnkart-gratone-cache/dd4a230e-0d02-4514-bab4-c7c279040c8c">Kartverket Gråtoner</a>'
  }
).addTo(map);

Matrikkelen: L.tileLayer
  .wms("https://openwms.statkart.no/skwms1/wms.matrikkel?", {
    format: "image/png",
    maxZoom: 20,
    transparent: true,
    opacity: 0.8,
    attribution:
      '<a href="https://kartkatalog.geonorge.no/metadata/kartverket/matrikkelen-wms/63c672fa-e180-4601-a176-6bf163e0929d">Matrikkelen</a>',
    layers: "eiendomsgrense,eiendoms_id"
  })
  .addTo(map), //enable Matrikkelen by default
  map.addControl(
    L.control.attribution({
      position: "bottomright",
      prefix: ""
    })
  );

map.attributionControl
  .addAttribution(
    'Laget av <a href="https://fredriklindseth.no/">Fredrik</a> til frende hackathon 2019'
  )
  .addTo(map);

var popup = L.popup();

function onMapClick(event) {
  let matrikkel = " and ";
  matrikkel += getProperty(event.latlng.lat, event.latlng.lng);
  popup
    .setLatLng(event.latlng)
    .setContent(
      `Du klikket på ${event.latlng.lat.toFixed(
        5
      )} / ${event.latlng.lng.toFixed(5)}${matrikkel}`
    )
    .openOn(map);
}

/// https://ws.geonorge.no/adresser/v1/#/default/get_punktsok
function getProperty(latitude, longtitude, searchRadius = 20) {
  console.log(
    `GET matrikkel for ${latitude.toFixed(5)} / ${longtitude.toFixed(5)}`
  );

  let url = `https://ws.geonorge.no/adresser/v1/punktsok?radius=${searchRadius}&lat=${latitude}&lon=${longtitude}&treffPerSide=10&side=0`;
  const request = new XMLHttpRequest();
  request.responseType = "json";
  request.open("GET", url, true);
  request.onload = function() {
    if (this.status == 200) {
      var response = request.response;

      if (response.metadata.totaltAntallTreff != 0) {
        let addressString = `${response.adresser["0"].adressenavn} ${
          response.adresser["0"].nummer
        }, ${response.adresser["0"].postnummer} ${
          response.adresser["0"].poststed
        }`;
        console.debug(addressString);
        let mnr = response.adresser["0"].kommunenummer;
        let gnr = response.adresser["0"].gardsnummer;
        let bnr = response.adresser["0"].bruksnummer;

        let matrikkel = `${mnr}-${gnr}-${bnr}`;
        console.log(`Returning ${matrikkel} and ${addressString}`);
        return { matrikkel, addressString };
      } else {
        console.log("No address returned or invalid format.");
        return null;
      }
    }
  };
  request.send();
}

map.on("click", onMapClick);
